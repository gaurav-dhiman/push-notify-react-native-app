import React, { Component } from 'react';
import { AppRegistry, Text } from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Title, List, ListItem, Button, Left, Right, Body, Icon } from 'native-base';
import { AppLoading } from 'expo';
import Meteor, { createContainer } from 'react-native-meteor';

const SERVER_URL = 'ws://192.168.0.3:3000/websocket';

export class App extends Component {
  state = {
    isReady: false,
  };

  async componentWillMount() {
    await Meteor.connect(SERVER_URL);  
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });
    this.setState({isReady: true});
  }

  renderRow(business) {
    return (
      <ListItem>
        <Text>{business.name}</Text>
      </ListItem>
    );
  }

  render() {
    if (!this.state.isReady || this.props.loading) {
      return <AppLoading />;
    }
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Header</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Text>
            Wow !! ... This is Content Section
          </Text>
          <Text>
            Total Businesses: {this.props.count}
          </Text>
          <List 
          dataArray={this.props.businesses}
          renderRow={this.renderRow}
          >
          </List>
          </Content>
        {/* <Footer>
          <FooterTab>
            <Button full>
              <Text>Footer</Text>
            </Button>
          </FooterTab>
        </Footer> */}
      </Container>
    );
  }
}

export default createContainer(() => {
  const businessSubscription = Meteor.subscribe('businesses');
  const businesses = Meteor.collection('business').find();
  console.log('Businesses Count: ' + businesses.length);
  return {
    loading: !businessSubscription.ready(),
    count: businesses.length,
    businesses: businesses
  };
}, App);